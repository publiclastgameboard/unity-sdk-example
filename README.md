## Unity SDK Examples

This project includes examples of how to use the various controllers in the Gameboard SDK. For more information, see our [documentation](https://external-codelabs.hosting.lastgameboard.com/).

You can either clone this repo to open in Unity, or download the Unity Package from the UnityPackage folder and pull that into an existing project.

## Unity Version:

    2021.3.1f1

## SDK Version:

    3.0.1
